import React from "react";
// @material-Ui:
import { withStyles } from "@material-ui/styles";
// Components:
import Gallary from "./Gallery";
import Footer from "./Footer";
import CategorySelect from "./features/CategorySelect";
import AddNewCategory from "./features/AddNewCategory";
// import CategorySelect from "./features/CategorySelect";
// Icons:
// import home from "./img/home.png";
// import bell from "./img/bell.png";
// import briefcase from "./img/briefcase.png";
import avatar from "./img/programmer.png";
// Styles:
import { styles } from "./styles/cardStyle";

const Card = (props) => {
  const { classes } = props;
  const [addNewCatSelected, setAddNewCatSelected] = React.useState(false);
  const [categoryItems, setCategoryItems] = React.useState([
    { value: "طراحی محصول" },
    { value: "مدیریت محصول" },
    { value: "هنر" },
  ]);

  const handleAddNewCategoryItem = (item) => {
    console.log(item);
    console.log(categoryItems);
    console.log(...categoryItems);
    console.log(...categoryItems, item);
    setCategoryItems([...categoryItems, item]);
    handleAddNewCategoryClose();
  };

  const handleAddNewCategoryOpen = () => {
    setAddNewCatSelected(true);
  };
  const handleAddNewCategoryClose = () => {
    setAddNewCatSelected(false);
  };
  return (
    <div className={classes.root}>
      <div className={classes.headerRoot}>
        <div className={classes.headerPic}>
          <img src={avatar} className={classes.avatarImg} alt="user" />
        </div>
        <div className={classes.headerChip}>
          {addNewCatSelected ? (
            <AddNewCategory
              addNewCategoryCloseHandler={handleAddNewCategoryClose}
              addNewCategoryItemHandler={handleAddNewCategoryItem}
            />
          ) : (
            <CategorySelect
              items={categoryItems}
              addNewCategoryOpenHandler={handleAddNewCategoryOpen}
            />
          )}
        </div>
      </div>
      <div className={classes.postRoot}>
        <div className={classes.postTitle}>عنوان مطلب</div>
        <div className={classes.postBody}>متن خود را تایپ کنید</div>
      </div>
      <div className={classes.galleryWrapper}>
        <Gallary />
      </div>
      <div className={classes.separator} />
      <div className={classes.footerRoot}>
        <Footer />
      </div>
    </div>
  );
};

export default withStyles(styles)(Card);
