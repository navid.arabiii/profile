import React from "react";
// Material-Ui:
import { withStyles } from "@material-ui/styles";
// Icons:
import home from "./img/home.png";
import bell from "./img/bell.png";
import briefcase from "./img/briefcase.png";
// Styles:
import { styles } from "./styles/footerStyles";

const Icon = ({
  classes,
  hover,
  mouseEnterHandler,
  mouseLeaveHandler,
  onClick,
  imgSrc,
  alt,
}) => (
  <div
    className={classes.iconRoot}
    onClick={onClick}
    onMouseEnter={mouseEnterHandler}
    onMouseLeave={mouseLeaveHandler}
  >
    <img
      src={imgSrc}
      className={classes.iconImg}
      style={hover ? {} : { filter: "grayscale(100%)" }}
      alt={alt}
    />
  </div>
);

const Footer = (props) => {
  const { classes } = props;
  const [hover, setHover] = React.useState(-1);

  return (
    <div className={classes.root}>
      <div className={classes.rightSide}>
        <Icon
          classes={classes}
          hover={hover === 0}
          mouseEnterHandler={() => setHover(0)}
          mouseLeaveHandler={() => setHover(-1)}
          onClick={() => console.log("home Btn Clicked")}
          imgSrc={home}
          alt={"home"}
        />
        <Icon
          classes={classes}
          hover={hover === 1}
          mouseEnterHandler={() => setHover(1)}
          mouseLeaveHandler={() => setHover(-1)}
          onClick={() => console.log("bell Btn Clicked")}
          imgSrc={bell}
          alt={"bell"}
        />
        <Icon
          classes={classes}
          hover={hover === 2}
          mouseEnterHandler={() => setHover(2)}
          mouseLeaveHandler={() => setHover(-1)}
          onClick={() => console.log("briefcase Btn Clicked")}
          imgSrc={briefcase}
          alt={"briefcase"}
        />
      </div>
      <div className={classes.btn}>انتشار</div>
    </div>
  );
};
export default withStyles(styles)(Footer);
