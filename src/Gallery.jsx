import React from "react";
// @material-Ui:
import { withStyles } from "@material-ui/styles";
import CloseBtn from "@material-ui/icons/Close";
// Images:
import firstImg from "./img/1.jpg";
import secondImg from "./img/2.jpg";
import thirdImg from "./img/3.jpg";
// Styles:
import { styles } from "./styles/galleryStyles";

const Gallery = (props) => {
  const [hover, setHover] = React.useState(-1);
  const [galleryArr, setArr] = React.useState([firstImg, secondImg, thirdImg]);
  const { classes } = props;

  const removeImg = (index) => {
    let newArr = [];
    if (galleryArr.length > 1) {
      newArr = galleryArr
        .slice(0, index)
        .concat(galleryArr.slice(index + 1, galleryArr.length));
      setArr(newArr);
      return;
    }
    console.log(
      "It is the last picture, so it is not rationale to remove it..."
    );
    return;
  };

  return (
    <div
      className={
        galleryArr.length === 3
          ? classes.root
          : galleryArr.length === 2
          ? classes.secondRoot
          : classes.thirdRoot
      }
    >
      {galleryArr.map((galleryItem, index) => (
        <div
          key={index}
          className={classes.itemRoot}
          style={
            index === 0
              ? { gridArea: "pic1" }
              : index === 1
              ? { gridArea: "pic2" }
              : { gridArea: "pic3" }
          }
          onMouseEnter={() => setHover(index)}
          onMouseLeave={() => setHover(-1)}
        >
          <img src={galleryItem} className={classes.itemImg} alt="" />

          <CloseBtn
            className={classes.closeBtn}
            style={
              galleryArr.length > 1
                ? hover === index
                  ? { opacity: "1", zIndex: "2" }
                  : { opacity: "0", zIndex: "-1" }
                : { opacity: "0", zIndex: "-1" }
            }
            onClick={() => removeImg(index)}
          />
        </div>
      ))}
    </div>
  );
};

export default withStyles(styles)(Gallery);
