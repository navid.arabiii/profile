import React, { useState } from "react";
// Material-Ui:
import { withStyles } from "@material-ui/styles";
import CloseIcon from "@material-ui/icons/Close";
// Styles:
import { styles } from "../styles/addNewCategoryStyles";

const AddNewCategory = (props) => {
  const { classes } = props;
  const [newCatInputValue, setNewCatInputValue] = useState("");

  return (
    <div className={classes.root}>
      <div className={classes.titleRoot}>
        <div className={classes.title}>دسته‌بندی جدید</div>
        <CloseIcon
          className={classes.close}
          onClick={() => props.addNewCategoryCloseHandler()}
        />
      </div>
      <input
        className={classes.input}
        value={newCatInputValue}
        onChange={(e) => setNewCatInputValue(e.target.value)}
      />
      <div
        className={classes.btn}
        onClick={() =>
          props.addNewCategoryItemHandler({ value: newCatInputValue })
        }
      >
        ایجاد
      </div>
    </div>
  );
};
export default withStyles(styles)(AddNewCategory);
