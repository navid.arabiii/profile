import React, { useState } from "react";
// Material-Ui:
import { withStyles } from "@material-ui/styles";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import AddIcon from "@material-ui/icons/Add";
// Styles:
import { styles } from "../styles/categoreSelectStyles";

const CategorySelect = (props) => {
  const { classes } = props;
  const [hover, setHover] = React.useState(false);
  const [showItems, setShowItems] = useState(false);
  const [selecedtOptionIndex, setSelectedOptionIndex] = useState(-1);
  const selectOptions = props.items || [];

  const handleOptionClick = (index) => {
    setSelectedOptionIndex(index);
    setShowItems(false);
  };

  console.log(props.items);
  console.log(selectOptions.length);

  return (
    <div
      className={classes.root}
      style={
        !showItems
          ? { height: "36px" }
          : selectOptions.length === 0
          ? { height: "36px" }
          : { height: `${selectOptions.length * 36 + 36 + 36 + 16}px` }
      }
    >
      <div
        className={classes.titleRoot}
        onClick={() => setShowItems(!showItems)}
      >
        <div className={classes.title}>
          {selecedtOptionIndex !== -1
            ? selectOptions[selecedtOptionIndex].value
            : showItems
            ? "دسته‌بندی جدید"
            : "انتخاب دسته‌بندی"}
        </div>
        <KeyboardArrowDownIcon
          className={classes.caret}
          style={showItems ? { transform: "rotate(180deg)" } : {}}
        />
      </div>
      {showItems && selectOptions.length > 0 && (
        <div className={classes.optionsRoot}>
          {selectOptions.map((option, index) => (
            <div
              key={index}
              className={classes.option}
              onClick={() => handleOptionClick(index)}
            >
              {option.value}
            </div>
          ))}
          <div
            className={classes.newCat}
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
            onClick={() => props.addNewCategoryOpenHandler()}
          >
            <span className={classes.option}>دسته‌بندی جدید</span>
            <AddIcon
              className={classes.plus}
              style={hover ? { transform: "rotate(180deg)" } : {}}
            />
          </div>
        </div>
      )}
    </div>
  );
};
export default withStyles(styles)(CategorySelect);
