export const styles = {
  root: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  rightSide: {
    width: "140px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  iconRoot: {
    width: "36px",
    height: "36px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: "3px",
    "&:hover": {
      backgroundColor: "#d8d8d8",
    },
  },
  iconImg: { width: "24px", height: "24px" },
  btn: {
    height: "36px",
    width: "56px",
    border: "none",
    borderRadius: "3px",
    padding: "0px 8px",
    margin: "0px 8px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontSize: "14px",
    color: "white",
    fontFamily: "IRSans",
    backgroundColor: "#3E92E6",
    "&:hover": {
      backgroundColor: "#d8d8d8",
    },
  },
};
